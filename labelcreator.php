<?php
/*
Plugin Name: Label Creator
 */

error_reporting(E_ALL ^ E_DEPRECATED);

require(__DIR__.'/admin.php');
require(__DIR__.'/spyc.php');

add_shortcode('labelcreator',function(){
	$labelcreator_fonts = array(
		'helvetica'=>'helvetica',
		'times'=>'times',
		'courier'=>'courier',
	);
	$labelcreator_corpses = array(
		'10'=>'Small',
		'12'=>'Medium',
		'14'=>'Large',
		'17'=>'XL',
		'20'=>'XXL',
		'23'=>'XXXL',
	);



	function select($name,$options){
		$html = "<select name='$name'>";
		foreach($options as $k=>$v) {
			$html .= "<option value='$k'>$v</option>";
		}
		$html .= "</select>";
		return $html;
	}

	return '<div role="form" lang="nl-NL" dir="ltr" style="max-width:400px;" class="labelcreator"><h1>Maak hier uw naamlabels</h1>'
		.'<form id="labelcreator_form" method="post" class="wpcf7-form" enctype="multipart/form-data" novalidate="novalidate">'
		.'<div id="labelcreator_select_container" data-labeltypes="'
		.htmlspecialchars(json_encode(spyc_load(get_option('labelcreator_klanten_yml'))),ENT_QUOTES).'"></div>'
		.'<label>Kies een lettertype</label>'
		.select('font',$labelcreator_fonts)
		.'<label>Kies een lettergrootte</label>'
		.select('corps',$labelcreator_corpses)
		.'<label>Vul de namenlijst in</label>'
		.'<textarea name="names" placeholder="Elke regel op een naambadge scheiden met een ; (punt komma)"></textarea><br>'
		.'<button type="submit" name="pdf" class="wpcf7-form-control wpcf7-submit">Maak PDF</button>'
		.'<div class="border"><h2>OPTIONEEL: Kies een excel bestand</h2><input type="file" name="bestand" size="40" class="wpcf7-form-control wpcf7-file" aria-invalid="false"><br><button class="button alt" id="uploadsheet" type="submit" name="excel" data-action="'.plugins_url('/sheetconv.php',__FILE__).'">naar namenlijst</button><br></div>'
		.'</form></div>';

});

/**
 * @brief Genereren van PDF
 */
function labelcreator_handle_submit(){
	if(!isset($_POST['pdf'])) return "";

	/*
	 * Sanitize
	 */
	$corps = (int)$_POST['corps'];

	/*
	 * Label type data opzoeken
	 */
	$typedata = spyc_load(get_option('labelcreator_klanten_yml'));
	while(count($_POST['type'])) {
		$typedata = $typedata[array_shift($_POST['type'])];
	}

	require("pdf.php");
	$pdf = new LabelCreatorPdf();
	$pdf->SetFont($_POST['font'],'',$_POST['corps']);
	$pdf->SetTextColor(0);
	$pdf->setTypeData($typedata);
	$pdf->AddPage();
	$pdf->drawLabels($_POST['names']);
	$pdf->Output();
	exit();
}
labelcreator_handle_submit();


function labelcreator_enqueue_script() {
	wp_enqueue_script('labelcreator',plugins_url('/labelcreator.js',__FILE__),'jquery');
}
add_action('wp_enqueue_scripts','labelcreator_enqueue_script');

