<?php

error_reporting(E_ALL ^ E_DEPRECATED);

require_once __DIR__ . '/phpExcel/PHPExcel.php';


/*
 * Sanity checks
 */
 //die(var_dump($_FILES['bestand']));
if(!isset($_FILES['bestand']))
	exit(json_encode(array('error'=>"Geen bestand geselecteerd")));
if($_FILES['bestand']['error'] != 0)
	exit(json_encode(array('error'=>"Upload fout")));
if(!preg_match('/\.(xls|xlsx|docx|csv)$/',$_FILES['bestand']['name']))
	exit(json_encode(array('error'=>"Dit is geen correct .xls of .xlsx bestand.")));
if(!is_readable($_FILES['bestand']['tmp_name']))
	exit(json_encode(array('error'=>"Geupload bestand kon niet geopend worden")));

/*
 * Spreadsheet reader
 */
 
 $tmpfile = $_FILES['bestand']['tmp_name'];
 $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpfile);
 $excelObj = $excelReader->load($tmpfile);
 $worksheet = $excelObj->getActiveSheet();
 $lastRow = $worksheet->getHighestRow();
 $text = '';
 
 for($row = 1; $row <= $lastRow; $row++){
	 $firstRow = $worksheet->getCell('A'.$row)->getValue();
	 $scndRow  = $worksheet->getCell('B'.$row)->getValue();
	 $text .= "{$firstRow};{$scndRow}\r\n";
 }

/*
 * Output as json
 */
$str = json_encode(array('success'=>trim($text)));
if($str===false) {
	exit(json_encode(array('error'=>"Foute encoding van sheet (utf-8 nodig)")));
}
exit($str);
