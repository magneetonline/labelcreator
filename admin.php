<?php

add_action('admin_menu',function(){
	$name = 'labelcreator_klanten_yml';

	add_options_page('LabelCreator Editor','LabelCreator Editor','manage_options','labelcreator-editor',function()use($name){
		if(!current_user_can('manage_options')) {
			wp_die(__('You do not have sufficient permissions to access this page.'));
		}

		if(isset($_POST[$name])) {
			
			update_option($name,stripslashes($_POST[$name]));
		}

		$css = get_option($name,'');
		$style = "
			
		";

		echo "<div class='wrap'>";
		echo "<h1>Labelcreator editor</h1>";
		echo "<div style='max-width:500px;float:left;margin-right:20px;'><p>Default waardes voor een label zijn:<br>breedte: 75<br>hoogte: 25<br>vensterbreedte: 0<br>vensterhoogte: 0<br>vensterlinks: 0<br>vensterboven: 0</p><p>Afmetingen zijn in mm.</p><p>'breedte' is de breedte van het label<br>'hoogte' is de hoogte van het label</p><p>voor 'vensterbreedte' geldt: als deze 0 is, wordt deze zo groot mogelijk gemaakt. Dat betekent dat het venster doorloopt tot de rechterkant van het label. Anders is dit de breedte van het venster in mm voor vensterhoogte geldt: als deze 0 is, wordt het venster zo groot mogelijk gemaakt. Anders is het de hoogte van het venster in mm.</p><p>'vensterlinks' is de marge met de linkerkant (x-positie) van het label<br>'vensterboven is de marge met de bovenkant (y-positie) van het label.</p><p>Deze configuratie is geschreven in <a href='http://yaml.org/'>Yaml</a> <a href='https://en.wikipedia.org/wiki/YAML'>Wikipedia</a></div>";

		echo "<form method='POST'>";
		echo "<div style='float:left;min-width:400px;'><textarea name='{$name}' id='{$name}' style='font-family:sans-serif;border: 1px solid #ddd;-webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.07);box-shadow: inset 0 1px 2px rgba(0,0,0,.07);background-color: #fff;color: #32373c;display:block;min-height:400px;max-height:80vh;overflow:scroll;margin-top:1em;width:500px;' contenteditable='true'>$css</textarea></div><br style='clear:both;'>";
		echo "<p class='submit'><input type='submit' name='submit' id='submit' class='button button-primary' value='Opslaan'></p>";
		echo "</form>";
		echo "</div>";
	});
});

/*
add_action('wp_footer',function(){
	$name = 'labelcreator_klanten_yml';
	echo "<style type='text/css'>";
	echo get_option($name,'');
	echo "</style>";
},100);
*/
