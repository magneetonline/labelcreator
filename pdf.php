<?php

require(__DIR__.'/fpdf/fpdf.php');

/**
 * @brief PDF klasse
 *
 * Deze klasse erft over van de FPDF library
 */
class LabelCreatorPdf extends FPDF {

	/* Afmetingen & afstanden */
	public $breedte = 75;
	public $hoogte = 25;
	public $vensterbreedte = 0;
	public $vensterhoogte = 0;
	public $vensterlinks = 0;
	public $vensterboven = 0;

	const MIN_PAGE_MARGIN_X=15;
	const MIN_PAGE_MARGIN_Y=15;

	/* Constructor */
	public function __construct() {
		parent::__construct('P','mm','A4');
	}

	/* Label type instellen */
	public function setTypeData($arr){
		$fields = array('breedte','hoogte','vensterlinks','vensterboven','vensterbreedte','vensterhoogte');
		foreach($fields as $f) {
			if(isset($arr[$f]))
				$this->{$f} = $arr[$f];
		}
		if($this->vensterbreedte == 0) $this->vensterbreedte = $this->breedte - $this->vensterlinks;
		if($this->vensterhoogte == 0) $this->vensterhoogte = $this->hoogte - $this->vensterboven;
	}

	public function drawLabels($rows) {

		$labelsPerRow = floor($this->w / ($this->breedte - self::MIN_PAGE_MARGIN_X*2));
		$pageMarginX = floor(($this->w % $this->breedte) / 2);
		$pageMarginY = floor(($this->h % $this->hoogte) / 2);

		$this->SetAutoPageBreak(false);

		$rows = iconv('UTF-8', 'windows-1252', $rows);

		$x = $pageMarginX;
		$y = $pageMarginY;
		foreach(explode("\n",$rows) as $i=>$row) {
			$lines = explode(';',trim($row));

			// Kader
			$this->SetXY($x,$y);
			$this->Cell($this->breedte,$this->hoogte,"",1,0,'C');

			/* Venster
			$this->SetXY($x+$this->vensterlinks,$y+$this->vensterboven);
			$this->SetFillColor(200);
			$this->Cell($this->vensterbreedte,$this->vensterhoogte,"",0,0,"",true);
			$this->SetFillColor(255);
			*/

			// Tekst
			$lineHeight = $this->vensterhoogte / count($lines);
			$this->SetXY($x+$this->vensterlinks, $y+$this->vensterboven);
			foreach($lines as $j=>$line)
				$this->Cell($this->vensterbreedte,$lineHeight,$line,0,2,'C');

			// Advance xy
			$x += $this->breedte;
			if($x+$this->breedte >= $this->w) {
				$x = $pageMarginX;
				$y += $this->hoogte;
			}

			// New page if appropriate
			if($y+$this->hoogte > $this->h) {
				$this->AddPage();
				$x = $pageMarginX;
				$y = $pageMarginY;
			}

	 	}
	}

}
