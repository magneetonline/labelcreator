/**
 * Label creator formulier javascript
 *
 * In dit bestand worden de select inputs voor het labelcreator formulier gegenereerd.
 * Dit script verwacht een div element met het id labelcreator_select_container, en
 * daarin een attribuut data-labeltypes. In dat attribuut moet de array van labeltypes worden opgenomen.
 *
 * De naam voor de select boxes is labelcreator_type[]. Dit is dus een array met een of meer select waarden
 */

// Begin jQuery alias '$'
(function($){

	var root;

	/**
	 * Bij laden van de pagina wordt dit stukje uitgevoerd.
	 * Omdat hier de DOM beschikbaar is kunnen we gegevens uit de HTML gaan
	 * lezen. Vervolgens wordt de eerste select aangemaakt.
	 */
	$(document).ready(function(){
		root = $('#labelcreator_select_container');
		if(!root.length) return;
		generateSelect(root.data('labeltypes'));
	});

	/* Deze functie maakt een select aan, voegt deze to aan de pagina,
	 * en configureert de event handlers
	 */
	function generateSelect(d){

		// Label aanmaken
		root.append($("<label/>").text(d.optie));
		
		// Select aanmaken
		var select = $("<select name='type[]'/>");
		for(var i in d) {
			if(i=='optie') continue;
			var option = $("<option/>").text(i).data('labeltypes',d[i]);
			select.append(option);
		}
		root.append(select);

		// Event handler toevoegen
		select.change(function(e){
			$(this).nextAll().remove();
			var children = $(this).find('option:selected').data('labeltypes');
			if(typeof children.optie == 'undefined') return;
			generateSelect(children);
		});
		select.change();
	}

	$(document).ready(function(){
		$("form [type=submit]").click(function() {
			$("[type=submit]").removeAttr("clicked");
			$(this).attr("clicked", "true");
		});
		$("#labelcreator_form").submit(function(e){
			if($("#uploadsheet[clicked=true]").length == 0) return;
			e.preventDefault();
			var form = document.getElementById('labelcreator_form');
			var fd = new FormData(form);

			$.ajax({
				'url':$("#uploadsheet").data('action'),
				'type':'POST',
				'processData':false,
				'contentType':false,
				'data':fd,
				'success':function(d){
					d = JSON.parse(d);
					if(typeof d.success != 'undefined') {
						$("textarea[name=names]").text(d.success);
					}
					else if(typeof d.error != 'undefined') {
						$(".uploaderr").remove();
						$("#uploadsheet").before("<div class='uploaderr'>"+d.error+"</div>");
					}
				}
			});

		});
	});

})(jQuery);
// Eind jQuery alias '$'
